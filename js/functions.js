
var GA_TYPE = {
    OPEN_INQUIRY_FORM: 1,
    SEND_INQUIRY_FORM: 2,
    THANK_POPUP      : 3
};

/**
 * Add bind function if not exist
 */
if (!Function.prototype.bind) {
    Function.prototype.bind = function (oThis) {
        if (typeof this !== 'function') {
            // closest thing possible to the ECMAScript 5
            // internal IsCallable function
            throw new TypeError('Function.prototype.bind - what is trying to be bound is not callable');
        }

        var aArgs = Array.prototype.slice.call(arguments, 1),
            fToBind = this,
            fNOP = function () {},
            fBound = function () {
                return fToBind.apply(this instanceof fNOP && oThis ? this : oThis, aArgs.concat(Array.prototype.slice.call(arguments)));
            };

        fNOP.prototype = this.prototype;
        fBound.prototype = new fNOP();

        return fBound;
    };
}

function isNumber(str) {
    var alphaExp = /^[0-9]+$/;
    if (str.match(alphaExp)) {
        return true;
    }
    return false;
}

function isMobile() {
    var user_agent = navigator.userAgent || navigator.vendor || window.opera;
    return /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(user_agent);
}

if (!String.prototype.format) {
    String.prototype.format = function () {
        var args = arguments;
        return this.replace(/{(\d+)}/g, function (match, number) {
            return typeof args[number] !== 'undefined'
                ? args[number]
                : match
                ;
        });
    };
}

/**
 * Get scrollbar width
 */
function getScrollBarWidth() {
    var inner = document.createElement('p');
    inner.style.width = "100%";
    inner.style.height = "200px";

    var outer = document.createElement('div');
    outer.style.position = "absolute";
    outer.style.top = "0px";
    outer.style.left = "0px";
    outer.style.visibility = "hidden";
    outer.style.width = "200px";
    outer.style.height = "150px";
    outer.style.overflow = "hidden";
    outer.appendChild(inner);

    document.body.appendChild(outer);
    var w1 = inner.offsetWidth;
    outer.style.overflow = 'scroll';
    var w2 = inner.offsetWidth;
    if (w1 == w2) {
        w2 = outer.clientWidth;
    }

    document.body.removeChild(outer);

    return (w1 - w2);
}

/**
 * Show popup at center of screen
 * @param {string} url
 * @param {string} title
 * @param {int} w
 * @param {int} h
 */
function popupCenter(url, title, w, h) {
    // Fixes dual-screen position
    var dualScreenLeft = window.screenLeft != undefined ? window.screenLeft : screen.left;
    var dualScreenTop = window.screenTop != undefined ? window.screenTop : screen.top;

    var width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;
    var height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;

    var left = ((width / 2) - (w / 2)) + dualScreenLeft;
    var top = ((height / 2) - (h / 2)) + dualScreenTop;
    var newWindow = window.open(url, title, 'scrollbars=yes, width=' + w + ', height=' + h + ', top=' + top + ', left=' + left + ', toolbar=0, location=0, menubar=0, directories=0, scrollbars=0');

    // Puts focus on the newWindow
    if (window.focus) {
        newWindow.focus();
    }
}

$.preloadImages = function () {
    for (var i = 0; i < arguments.length; i++) {
        $("<img />").attr("src", arguments[i]);
    }
};

/**
 * Send event to Google Analytics
 * @param {GA_TYPE} type
 */
function addGaEvent(type) {
    try {
        if (type === GA_TYPE.OPEN_INQUIRY_FORM) {
            // when open popup form.
            ga('send', 'event', 'Button', 'Click', 'Open Inquiry form');
        } else if (type === GA_TYPE.SEND_INQUIRY_FORM) {
            // when submit button clicked
            ga('send', 'event', 'Button', 'Click', 'Send Inquiry form');
        } else if (type === GA_TYPE.THANK_POPUP) {
            // when thanks popup shown.
            ga('send', 'event', 'Popup', 'Show', 'Thanks popup');
        }
    } catch (e) {
        // Oop
    }
}

/**
 * Get corss document height
 * @returns number
 */
function getDocumentHeight() {
    return Math.max(
        Math.max(document.body.scrollHeight, document.documentElement.scrollHeight),
        Math.max(document.body.offsetHeight, document.documentElement.offsetHeight),
        Math.max(document.body.clientHeight, document.documentElement.clientHeight)
    );
}

/**
 * 
 * @returns int
 */
function getInnerWidth(withScroolBar) {
    if (withScroolBar) {
        return window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
    } else {
        return $('body').innerWidth();
    }
}

/**
 * 
 * @returns int
 */
function getInnerHeight(withScroolBar) {
    if (withScroolBar) {
        return window.innerHeight || document.documentElement.clientHeight || document.body.clientHeight;
    } else {
        return $('body').innerHeight();
    }
}
