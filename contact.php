<?php
    ob_start();
    session_start();
    $params = array();
    $error = array();
    $step = 1;

    if ($_SERVER['REQUEST_METHOD'] === 'POST') {
        $params = $_POST;

        if (empty($params['company_name'])) {
            $error['company_name'] = '※ Please enter company name';
        }
        if (empty($params['person_charge'])) {
            $error['person_charge'] = '※ Please enter the person in charge name';
        }
        if (empty($params['tel'])) {
            $error['tel'] = '※ Please enter phone number';
        }
        if (empty($params['email'])) {
            $error['email'] = '※Please enter your e-mail address';
        } else if (!filter_var($params['email'], FILTER_VALIDATE_EMAIL)) {
            $error['email'] = 'E-mail address format is incorrect';
        }
        if (empty($params['content'])) {
            $error['content'] = '※ Please enter contents';
        }
        if (empty($error)) {
            if (!empty($params['send'])) {
                // Send mail
                require_once 'contact_send_mail.php';
                $error_debug = '';
                $check = send_contact_mail($params, $error_debug);
                if ($check) {
                    $_SESSION['params'] = NULL;
                    header("Location: contact.php?complete=1");
                    die();
                } else {
                    $error['send'] = 'Failed to send mail';
                }
            } else {
                // Confirm
                $_SESSION['params'] = $params;
                $step = 2;
            }
        }
    } else {
        if (!empty($_GET['back']) && !empty($_SESSION['params'])) {
            $params = $_SESSION['params'];
        }
        if (!empty($_GET['complete'])) {
            $step = 3;
        }
    }
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Laboratory development · Team sourcing | OceanHawks |</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
        <meta name="description" content="あなたの会社の開発チームがグローバルに。OceanHawks (オーシャンホークス)は、開発リソースの不足や人員計画などの悩みを解決し、グローバル展開を見据えた開発をバングラデシュエンジニアのチームソーシングにより安価・高品質に行うことができます。"/>
        <meta name="keywords" content="ラボ開発, ラボ契約, オフショア, アジア, バングラデシュ, アウトソーシング, グローバル, web制作, 人件費, エンジニア, iOS, Android"/>
        <link href="favicon.ico" type="image/x-icon" rel="icon"/>
        <link href="favicon.ico" type="image/x-icon" rel="shortcut icon"/>

        <meta property="og:title" content="OceanHawks"/>
        <meta property="og:type" content="website"/>
        <meta property="og:url" content="https://oceanhawks.co.jp/"/>
        <meta property="og:image"  content="http://webhawks.oceanize.co.jp/oceanhawks/lp/img/oceanHawks_facebook_share.png"/>
        <meta property="og:description" content="あなたの会社の開発チームがグローバルに。OceanHawks (オーシャンホークス)は、開発リソースの不足や人員計画などの悩みを解決し、グローバル展開を見据えた開発をバングラデシュエンジニアのチームソーシングにより安価・高品質に行うことができます。"/>
        <meta property="og:site_name" content="OceanHawks"/>
        <meta property="og:locales" content="ja_JP"/>
        <meta property="fb:app_id" content="1223243807710456"/>

        <link rel="stylesheet" href="css/bootstrap.min.css">
        <link rel="stylesheet" href="css/style.css?20160803">
        <link rel="stylesheet" href="css/style_sp.css?20160803" media="only screen and (max-width:750px)"/>
    </head>
    <body>
        <!-- for facebook SDK -->
        <script>
          window.fbAsyncInit = function() {
            FB.init({
              appId      : '1223243807710456',
              xfbml      : true,
              version    : 'v2.7'
            });
          };

          (function(d, s, id){
             var js, fjs = d.getElementsByTagName(s)[0];
             if (d.getElementById(id)) {return;}
             js = d.createElement(s); js.id = id;
             js.src = "//connect.facebook.net/en_US/sdk.js";
             fjs.parentNode.insertBefore(js, fjs);
           }(document, 'script', 'facebook-jssdk'));
        </script>
        <!-- for facebook SDK end -->

        <nav class="navbar navbar-inverse navbar-fixed-top shadow">
            <div class="container">
                <h1 class="no_margin">
                    <a class="navbar-logo" href="/">
                        <img src="img/logo.png" alt="株式会社オーシャンホークス"/>
                    </a>
                </h1>
            </div>
        </nav>

        <div class="container" id="contact_header">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <div id="contact_header_detail">
                        <div>
                            CONTACT<br/>
                            <span>Inquiry form</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="container" id="contact_nav">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <div id="contact_nav_detail">
                        <table>
                            <tr>
                                <td class="<?php if($step == 1) echo 'active' ?>">1</td>
                                <td></td>
                                <td class="<?php if($step == 2) echo 'active' ?>">2</td>
                                <td></td>
                                <td class="<?php if($step == 3) echo 'active' ?>">3</td>
                            </tr>
                            <tr>
                                <td class="<?php if($step == 1) echo 'active' ?>">Enter necessary information</td>
                                <td>
                                    <img src="img/3arrow_gray.png" alt=""/>
                                </td>
                                <td class="<?php if($step == 2) echo 'active' ?>">Confirmation of input contents</td>
                                <td>
                                    <img src="img/3arrow_gray.png" alt=""/>
                                </td>
                                <td class="<?php if($step == 3) echo 'active' ?>">send completely</td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        <?php if(!empty($error_debug)): ?>
        <div class="hidden">
            <pre>
                <?php print_r($error_debug) ?>
            </pre>
        </div>
        <?php endif; ?>

        <div class="container" id="contact_detail">
            <div class="row">
                <div class="col-lg-12">
                    <div id="contact_detail_container">
                        <?php if($step == 1): ?>
                        <div id="contact_detail_alert" class="<?php if (empty($error)) echo 'hidden' ?>">
                            <span>
                                <?php
                                    if (!empty($error['send'])) {
                                        echo $error['send'];
                                    } else {
                                        echo 'Please check the input content';
                                    }
                                ?>
                            </span>
                        </div>
                        <div id="contact_detail_notice"><span>※ All fields are required</span></div>

                        <form id="contact_form" action="contact.php" method="POST">
                            <div class="form-group <?php if (!empty($error['company_name'])) echo 'has-error' ?>" id="frm_g_company_name">
                                <div>
                                    <label for="frm_company_name">Company name</label>
                                    <p class="help-block pull-right"><?php if (!empty($error['company_name'])) echo $error['company_name'] ?></p>
                                </div>
                                <input name="company_name" type="text" class="form-control" id="frm_company_name" placeholder="
Example) Ocean Hawks Co., Ltd." value="<?php if(!empty($params['company_name'])) echo htmlspecialchars($params['company_name']) ?>"/>
                            </div>
                            <div class="form-group <?php if (!empty($error['person_charge'])) echo 'has-error' ?>" id="frm_g_person_charge">
                                <div>
                                    <label for="frm_person_charge">Contact name</label>
                                    <p class="help-block pull-right"><?php if (!empty($error['person_charge'])) echo $error['person_charge'] ?></p>
                                </div>
                                <input name="person_charge" type="text" class="form-control" id="frm_person_charge" placeholder="
Example) Taro Hawks" value="<?php if(!empty($params['person_charge'])) echo htmlspecialchars($params['person_charge']) ?>"/>
                            </div>
                            <div class="form-group <?php if (!empty($error['tel'])) echo 'has-error' ?>" id="frm_g_tel">
                                <div>
                                    <label for="frm_tel">Phone number</label>
                                    <p class="help-block pull-right"><?php if (!empty($error['tel'])) echo $error['tel'] ?></p>
                                </div>
                                <input name="tel" type="text" class="form-control" id="frm_tel" placeholder="
Example) 03-1234-56780" value="<?php if(!empty($params['tel'])) echo htmlspecialchars($params['tel']) ?>"/>
                            </div>
                            <div class="form-group <?php if (!empty($error['email'])) echo 'has-error' ?>" id="frm_g_email">
                                <div>
                                    <label for="frm_email">Mail address</label>
                                    <p class="help-block pull-right"><?php if (!empty($error['email'])) echo $error['email'] ?></p>
                                </div>
                                <input name="email" type="text" class="form-control" id="frm_email" placeholder="E.g.) example@example.com" value="<?php if(!empty($params['email'])) echo htmlspecialchars($params['email']) ?>"/>
                            </div>
                            <div class="form-group <?php if (!empty($error['content'])) echo 'has-error' ?>" id="frm_g_content">
                                <div>
                                    <label for="frm_content">Request / consultation content</label>
                                    <p class="help-block pull-right"><?php if (!empty($error['content'])) echo $error['content'] ?></p>
                                </div>
                                <textarea name="content" id="frm_content" class="form-control" rows="3"><?php if(!empty($params['content'])) echo htmlspecialchars($params['content']) ?></textarea>
                            </div>

                            <button type="submit" class="btn btn-block">To confirmation screen</button>
                        </form>
                        <?php endif; ?>

                        <?php if($step == 2): ?>
                        <table>
                            <tr>
                                <td>Company name</td>
                                <td><?php echo $params['company_name'] ?></td>
                            </tr>
                            <tr>
                                <td>Contact name</td>
                                <td><?php echo $params['person_charge'] ?></td>
                            </tr>
                            <tr>
                                <td>Phone number</td>
                                <td><?php echo $params['tel'] ?></td>
                            </tr>
                            <tr>
                                <td>Mail address</td>
                                <td><?php echo $params['email'] ?></td>
                            </tr>
                            <tr>
                                <td>Request / consultation content</td>
                                <td><?php echo nl2br($params['content']) ?></td>
                            </tr>
                        </table>

                        <div id="contact_back">
                            <a href="?back=1">Correct the input contents</a>
                        </div>

                        <form action="contact.php" method="POST">
                            <input type="hidden" name="company_name" value="<?php echo $params['company_name'] ?>"/>
                            <input type="hidden" name="person_charge" value="<?php echo $params['person_charge'] ?>"/>
                            <input type="hidden" name="tel" value="<?php echo $params['tel'] ?>"/>
                            <input type="hidden" name="email" value="<?php echo $params['email'] ?>"/>
                            <textarea class="hidden" name="content"><?php echo $params['content'] ?></textarea>
                            <input type="hidden" name="send" value="1"/>
                            <button type="submit" class="btn btn-block" onclick="addGaEvent(GA_TYPE.SEND_INQUIRY_FORM)">Send with the above contents</button>
                        </form>
                        <?php endif; ?>

                        <?php if($step == 3): ?>
                        <div id="contact_complete">
                            <div>
                                <span>
                                  Transmission is completed. Thank you for your inquiry. I will contact you later from charge.
                                </span>
                            </div>

                            <a href="http://webhawks.oceanize.co.jp/oceanhawks/lp">
                                <button type="button" class="btn btn-block">Back to TOP page</button>
                            </a>
                        </div>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>

        <div class="container" id="area_footer">
            <div class="row">
                <div class="col-lg-12">copyright ©  2016 OceanHawks</div>
            </div>
        </div>

        <script type="text/javascript" src="js/jquery-2.1.3.min.js"></script>
        <script type="text/javascript" src="js/jquery-migrate-1.4.1.min.js"></script>
        <script type="text/javascript" src="js/bootstrap.min.js"></script>
        <script type="text/javascript" src="js/jquery-ui.min.js"></script>
        <script type="text/javascript" src="js/modernizr-custom.js"></script>
        <script type="text/javascript" src="js/jquery.kerning.min.js"></script>
        <script type="text/javascript" src="js/parallax.min.js"></script>
        <script type="text/javascript" src="js/wow.min.js"></script>
        <script type="text/javascript" src="js/jquery.appear.js"></script>
        <script type="text/javascript" src="js/functions.js?20160803"></script>
        <script type="text/javascript" src="js/common.js?20160803"></script>
        <script>
            (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
            })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

            ga('create', 'UA-81271356-1', 'auto');
            ga('send', 'pageview');

            $(document).ready(function(){
                <?php if($step == 3): ?>
                addGaEvent(GA_TYPE.THANK_POPUP);
                <?php endif; ?>
            });
        </script>
        <!-- Chamo script begin -->
        <script>
            var _chaq = _chaq || [];
            _chaq['_accountID']=2521;
            (function(D,s){
                var ca = D.createElement(s)
                ,ss = D.getElementsByTagName(s)[0];
                ca.type = 'text/javascript';
                ca.async = !0;
                ca.setAttribute('charset','utf-8');
                var sr = 'https://v1.chamo-chat.com/chamovps.js';
                ca.src = sr + '?' + parseInt((new Date)/60000);
                ss.parentNode.insertBefore(ca, ss);
            })(document,'script');
        </script>
        <!-- Chamo script end -->
    </body>
</html>
