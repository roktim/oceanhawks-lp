
var intervalScroll = null;
var isFinishLastTopAnimation = false;
var isScrollToBottom = false;
var is_mobile = isMobile();
var wow = null;

$(document).ready(function(){
    $(this).scrollTop(0);
    
    // Top slide
    if ($('#myCarousel').length > 0) {
        $('#myCarousel').carousel();
    }
    
    // Paralax slider
    setBanerHeight();
    
    // WOW animation
    initWow();
    
    // Last animation of Top 5 items
    setTimeout(function () {
        $('#area_1_item_5').on("animationend webkitAnimationEnd oAnimationEnd MSAnimationEnd", function (e) {
            if ($('#area_1_item_5').css('visibility') != 'hidden') {
                isFinishLastTopAnimation = true;
                showMore();
            }
        });
    }, 1);
    
    // Hover area_6
    $('#area_4_detail td').hover(function () {
        if ($(this).hasClass('area_4_table_col3')) {
            $('.area_4_table_col3').each(function(){
                $(this).addClass('area_4_td_hover');
            });
        }
        if ($(this).hasClass('area_4_table_col4')) {
            $('.area_4_table_col4').each(function(){
                $(this).addClass('area_4_td_hover');
            });
        }
        if ($(this).hasClass('area_4_table_col5')) {
            $('.area_4_table_col5').each(function(){
                $(this).addClass('area_4_td_hover');
            });
        }
    }, function () {
        $('.area_4_td_hover').each(function(){
            $(this).removeClass('area_4_td_hover');
        });
    });
    
    // Show more when click in >>>
    $('#area_1_more_container').on('click', function(){
        showMore(true);
        
        var headerHeight = $('.navbar-fixed-top').height();
        var ele = $('#area_2_container');

        $('html, body').animate({
            scrollTop: ele.offset().top - headerHeight
        }, 400, 'easeInQuart');
    });
    
});

function setBanerHeight() {
    var sliderHeight = $(window).height() - $('.navbar-fixed-top').height();
    //var sliderHeight = 552;
    $('#myCarousel')
    .height(sliderHeight)
    .find('.carousel-inner').height(sliderHeight)
    .find('.item').each(function () {
        $(this)
        .height(sliderHeight)
        .find('.parallax-window').height(sliderHeight);
    });
}

$.preloadImages(
    "img/contact_text_blue.png", "img/contact_text.png",
    "img/arrow_contact_blue.png", "img/arrow_contact.png",
    "img/more_b.png",
    "img/arrow_more_b.png"
);

var initWow = function(){
    $(function () {
        fixAnimationMobile();
        
        wow = new WOW({
            boxClass: 'wow',            // animated element css class (default is wow)
            animateClass: 'animated',   // animation css class (default is animated)
            offset: 100,                // distance to the element when triggering the animation (default is 0)
            mobile: true                // trigger animations on mobile devices (true is default)
        });
        wow.init();
    });
};

var fixAnimationMobile = function(){
    if (is_mobile || getInnerWidth(true) <= 750) {
        $('.fadeInUp').each(function(){
            $(this).removeClass('fadeInUp').addClass('fadeInUpMobile');
        });
    } else {
        $('.fadeInUpMobile').each(function(){
            $(this).removeClass('fadeInUpMobile').addClass('fadeInUp');
        });
    }
    
    if (typeof wow !== 'undefined' && wow) {
        wow.resetStyle();
    }
};

var showMore = function(now){
    if ($('#stop_scroll').hasClass('hidden')) {
        if (now) {
            // Show now
            isFinishLastTopAnimation = true;
            isScrollToBottom = true;
            
            $('#stop_scroll').removeClass('hidden');
            $('#stop_scroll').appear();
            
            // Disable wow animation that animated
            $('#area_1 .wow').each(function () {
                $(this).removeClass('wow');
            });
            initWow();
        } else {
            // Show after x seconds
            if (isFinishLastTopAnimation && isScrollToBottom) {
                clearTimeout(intervalScroll);
                intervalScroll = setTimeout(function () {
                    showMore(true);
                }, 250);
            }
        }
    }
};

$(window).resize(function () {
    setBanerHeight();
    fixAnimationMobile();
});

$(window).on('beforeunload', function () {
    $(window).scrollTop(0);
});

/* Show / hide MORE element */
$(document).on('disappear', '#stop_scroll', function (e) {
    $('#stop_scroll').addClass('hidden');
    
    isFinishLastTopAnimation = false;
    $('.area_1_item').addClass('wow');
    initWow();
});

$(window).scroll(function (e) {
    if ($(window).scrollTop() + $(window).height() >= $(document).height()) {
        isScrollToBottom = true;
        showMore();
    }
    
    // Parallax effect
    var parallax = document.querySelectorAll(".parallax_item");
    [].slice.call(parallax).forEach(function (el, i) {
        var windowYOffset = window.pageYOffset;
        el.style.top = (windowYOffset - (windowYOffset / 2)) + 'px';
    });
});
