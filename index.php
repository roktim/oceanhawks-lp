<?php
    // Get host url
    $s = $_SERVER;
    $ssl = (!empty($s['HTTPS']) && $s['HTTPS'] == 'on');
    $sp = strtolower($s['SERVER_PROTOCOL']);
    $protocol = substr($sp, 0, strpos($sp, '/')) . (($ssl) ? 's' : '');
    $port = $s['SERVER_PORT'];
    $port = ((!$ssl && $port == '80') || ($ssl && $port == '443')) ? '' : ':' . $port;
    $host = ($use_forwarded_host && isset($s['HTTP_X_FORWARDED_HOST'])) ? $s['HTTP_X_FORWARDED_HOST'] : (isset($s['HTTP_HOST']) ? $s['HTTP_HOST'] : null);
    $host = isset($host) ? $host : $s['SERVER_NAME'] . $port;
    $url = $protocol . '://' . $host;
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Laboratory development · Team sourcing | OceanHawks |</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
        <meta name="description" content="あなたの会社の開発チームがグローバルに。OceanHawks (オーシャンホークス)は、開発リソースの不足や人員計画などの悩みを解決し、グローバル展開を見据えた開発をバングラデシュエンジニアのチームソーシングにより安価・高品質に行うことができます。"/>
        <meta name="keywords" content="オーシャンホークス,ラボ開発, ラボ契約, オフショア, アジア, バングラデシュ, アウトソーシング, グローバル, web制作, 人件費, エンジニア, iOS, Android"/>
        <meta name="google-site-verification" content="EhoXJvT3mbr0WIKUoz2dybQ_56BG5eTMx3q9OWK0qF0" />
        <link href="favicon.ico" type="image/x-icon" rel="icon"/>
        <link href="favicon.ico" type="image/x-icon" rel="shortcut icon"/>

        <meta property="og:title" content="株式会社オーシャンホークス（OceanHawks,Inc.）"/>
        <meta property="og:type" content="website"/>
        <meta property="og:url" content="https://oceanhawks.co.jp/"/>
        <meta property="og:image"  content="<?php echo $url ?>/img/oceanHawks_facebook_share.png"/>
        <meta property="og:description" content="あなたの会社の開発チームがグローバルに。OceanHawks (オーシャンホークス)は、開発リソースの不足や人員計画などの悩みを解決し、グローバル展開を見据えた開発をバングラデシュエンジニアのチームソーシングにより安価・高品質に行うことができます。"/>
        <meta property="og:site_name" content="株式会社オーシャンホークス（OceanHawks,Inc.）"/>
        <meta property="og:locales" content="ja_JP"/>
        <meta property="fb:app_id" content="1223243807710456"/>

        <link rel="stylesheet" href="css/bootstrap.min.css">
        <link rel="stylesheet" href="css/animate.css?20160913">
        <link rel="stylesheet" href="css/style.css?20160913">
        <link rel="stylesheet" href="css/style_sp.css?20160913" media="only screen and (max-width:750px)"/>
        <link rel="stylesheet" type="text/css" href="css/style_ie.css"/>
    </head>
    <body>
        <!-- for facebook SDK -->
        <script>
          window.fbAsyncInit = function() {
            FB.init({
              appId      : '1223243807710456',
              xfbml      : true,
              version    : 'v2.7'
            });
          };

          (function(d, s, id){
             var js, fjs = d.getElementsByTagName(s)[0];
             if (d.getElementById(id)) {return;}
             js = d.createElement(s); js.id = id;
             js.src = "//connect.facebook.net/en_US/sdk.js";
             fjs.parentNode.insertBefore(js, fjs);
           }(document, 'script', 'facebook-jssdk'));
        </script>
        <!-- for facebook SDK end -->

        <nav class="navbar navbar-inverse navbar-fixed-top shadow">
            <div class="container">
                <h1 class="no_margin">
                    <a class="navbar-logo" href="/">
                        <img src="img/logo.png" alt="Ocean Hawks Co., Ltd."/>
                    </a>
                </h1>
                <a class="navbar-contact" href="contact.php" onclick="addGaEvent(GA_TYPE.OPEN_INQUIRY_FORM)">
                    <div id="navbar-contact-ani"><span></span><span></span><span></span></div>
                    <img src="img/header_contact_2.png" alt=""/>
                </a>

                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed dropdown active" data-toggle="collapse" data-target="#bs-navbar-collapse-1">
                        <span class="sr-only">Menu</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                </div>
                <div class="collapse navbar-collapse shadow_bottom" id="bs-navbar-collapse-1">
                    <ul class="nav navbar-nav">
                        <li>
                            <a href="contact.php" onclick="addGaEvent(GA_TYPE.OPEN_INQUIRY_FORM)">
                                CONTACT US
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>

        <div id="myCarousel" class="carousel slide" data-ride="carousel">
            <div class="carousel-overlay"></div>

            <ol class="carousel-indicators">
                <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                <li data-target="#myCarousel" data-slide-to="1" class=""></li>
                <li data-target="#myCarousel" data-slide-to="2" class=""></li>
            </ol>

            <div class="carousel-inner">
                <div class="item parallax_item active" style="background-image: url(img/slider/1.png)"></div>
                <div class="item parallax_item" style="background-image: url(img/slider/2.jpg)"></div>
                <div class="item parallax_item" style="background-image: url(img/slider/3.jpg)"></div>
            </div>

            <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
                <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
                <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>

            <div class="carousel-caption">
                <div>
                    <h2>Team Sourcing (Laboratory development)</h2>
                    <h2>Sourcing the team as it is!</h2>
                    <h2>Your company's development team globally.</h2>
                    <a href="contact.php" onclick="addGaEvent(GA_TYPE.OPEN_INQUIRY_FORM)">
                        <div class="display_table">
                            <div class="display_row">
                                <div class="display_cell"><div></div></div>
                                <div class="display_cell"><span></span></div>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
        </div>

        <div id="area_1_container">
            <div class="container" id="area_1">
                <div class="row">
                    <h2 class="no_margin col-lg-12" id="area_1_title">Are you having trouble with the following things?</h2>
                </div>
                <div class="row">
                    <div class="col-lg-12" id="area_1_row_1">
                        <div class="area_1_item wow fadeInUp" id="area_1_item_1" data-wow-delay="0s">Domestic development resources are not enough
                        </div>
                        <div class="area_1_item wow fadeInUp" id="area_1_item_2" data-wow-delay="0.3s">
                          Develop development personnel as planned I can not adopt it
                        </div>
                        <div class="area_1_item wow fadeInUp" id="area_1_item_3" data-wow-delay="0.6s">Delivery due to overtime such as overtime I have covered it
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12" id="area_1_row_1">
                        <div class="area_1_item wow fadeInUp" id="area_1_item_4" data-wow-delay="0.9s">
                          To be globally accepted
                          I want to make a service someday
                        </div>
                        <div class="area_1_item wow fadeInUp" id="area_1_item_5" data-wow-delay="1.2s">
                  Development cost and
                  I want to reduce operating costs
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12" id="area_1_more_container">
                        <div id="area_1_more"><span></span><span></span><span></span></div>
                    </div>
                </div>
            </div>
        </div>

        <div id="stop_scroll" class="hidden">
            <div id="area_2_container">
                <div class="container" id="area_2">
                    <div class="row">
                        <h2 class="no_margin col-lg-12" id="area_2_title">Team Sourcing (Lab type development) will be solved!</h2>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="area_2_item shadow" id="area_2_item_1">
                                <div class="area_2_item_icon crisp"></div>
                                <div class="area_2_item_text">
                                  Global
                                  Excellent talent
                                  It can be adopted according to plan
                                </div>
                            </div>
                            <div class="area_2_item shadow" id="area_2_item_2">
                                <div class="area_2_item_icon crisp"></div>
                                <div class="area_2_item_text">
                                  A diligent development team
                                  Ensure to digest tasks
                                </div>
                            </div>
                            <div class="area_2_item shadow" id="area_2_item_3">
                                <div class="area_2_item_icon crisp"></div>
                                <div class="area_2_item_text">

                                  Compared with domestic labor costs
                                  With cheap expenses
                                  Can be developed and operated
                                </div>
                            </div>
                            <div class="area_2_item shadow" id="area_2_item_4">
                                <div class="area_2_item_icon crisp"></div>
                                <div class="area_2_item_text">
                                  Global
                                  To the development team
                                  Evolution and growth can be expected
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="container" id="area_3">
                <div class="row">
                    <h2 class="no_margin col-lg-12" id="area_3_title">
                        Team Sourcing (Laboratory Development)
                        <span>
                        As an exclusive development team of your company overseas engineers will work on development for a certain period of time.
                        This will solve your problem.
                        </span>
                    </h2>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div id="area_3_detail">
                            <img src="img/area3.png" alt="What's is Team Sourcing?"/>
                        </div>
                    </div>
                </div>
            </div>

            <div class="container" id="area_4">
                <div class="row">
                    <h2 class="no_margin col-lg-12" id="area_4_title">Team Sourcing Comparison of unit price sense and feature of laboratory development (laboratory development)</h2>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div id="area_4_detail">
                            <div class="shadow">
                                <table>
                                    <tr class="area_4_tr_header">
                                        <td class="area_4_table_col1"></td>
                                        <td colspan="2" class="area_4_table_col2 area_4_td_active">

                                          Team sousing
                                          (Lab type) Development
                                        </td>
                                        <td colspan="2" class="area_4_table_col3">
                                          At own company (domestic)
                                          When hiring
                                        </td>
                                        <td colspan="2" class="area_4_table_col4">When dispatching is used</td>
                                        <td colspan="2" class="area_4_table_col5">

                                        Make overseas bases on your own
                                        To launch
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Programmer unit price</td>
                                        <td colspan="2" class="area_4_td_active">
                                          80,000 yen / month
                                          (Human day 4000 yen ~)
                                        </td>
                                        <td colspan="2" class="area_4_table_col3">

                                          500,000 yen / month
                                          (Personnel expenses + overhead expenses)
                                        </td>
                                        <td colspan="2" class="area_4_table_col4">
                                          800,000 yen / month (40,000 yen / day from people)
                                        </td>
                                        <td colspan="2" class="area_4_table_col5">Depends on country, region, etc.</td>
                                    </tr>
                                    <tr>
                                        <td rowspan="2">Characteristic</td>
                                        <td class="area_4_td_active area_4_td_mark area_4_td_no_border_bottom area_4_td_no_border_right">◯</td>
                                        <td class="area_4_td_active text-left area_4_td_no_border_bottom">
                                          From abundant overseas resources
                                          Until you meet the right person
                                          I will continue to introduce
                                        </td>
                                        <td class="area_4_td_mark area_4_td_no_border_bottom area_4_td_no_border_right area_4_table_col3">△</td>
                                        <td class="text-left area_4_td_no_border_bottom area_4_table_col3">
                                          To be taken to competition
                                          Excellent talent
                                          It is difficult to secure
                                        </td>
                                        <td class="area_4_td_mark area_4_td_no_border_bottom area_4_td_no_border_right area_4_table_col4">△</td>
                                        <td class="text-left area_4_td_no_border_bottom area_4_table_col4">
                                          Can interview There are few candidates
                                        </td>
                                        <td class="area_4_td_mark area_4_td_no_border_bottom area_4_td_no_border_right area_4_table_col5">△</td>
                                        <td class="text-left area_4_td_no_border_bottom area_4_table_col5">
                                          Starting overseas bases I have no experienced people
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="area_4_td_active area_4_td_mark area_4_td_no_border_right">◯</td>
                                        <td class="area_4_td_active text-left">
                                        Because it can handle variable costs
                                        Flexible response to management strategy
                                        </td>
                                        <td class="area_4_td_mark area_4_td_no_border_right area_4_table_col3">△</td>
                                        <td class="text-left area_4_table_col3">
                                          Talent attracting media
                                          The trouble of posting management
                                        </td>
                                        <td class="area_4_td_mark area_4_td_no_border_right area_4_table_col4">△</td>
                                        <td class="text-left area_4_table_col4">Introduction cost incurred</td>
                                        <td class="area_4_td_mark area_4_td_no_border_right area_4_table_col5">△</td>
                                        <td class="text-left area_4_table_col5">
                                          Work on actual work
                                          Cost plans up to
                                          Difficult to set up
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="container" id="area_5">
                <div class="row">
                    <h2 class="no_margin col-lg-12" id="area_5_title">Examples of engineer skills that can be introduced</h2>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div id="area_5_detail">
                            <div class="area_5_item shadow"><span>Android</span></div>
                            <div class="area_5_item shadow"><span>C</span></div>
                            <div class="area_5_item shadow"><span>CakePHP</span></div>
                            <div class="area_5_item shadow"><span>CSS</span></div>
                            <div class="area_5_item shadow"><span>Git</span></div>
                            <div class="area_5_item shadow"><span>HTML</span></div>

                            <div class="area_5_item shadow"><span>Illustrator</span></div>
                            <div class="area_5_item shadow"><span>Java</span></div>
                            <div class="area_5_item shadow"><span>JavaScript</span></div>
                            <div class="area_5_item shadow"><span>LAMP</span></div>
                            <div class="area_5_item shadow"><span>MySQL</span></div>
                            <div class="area_5_item shadow"><span>Objective-C</span></div>

                            <div class="area_5_item shadow"><span>Oracle Database</span></div>
                            <div class="area_5_item shadow"><span>Perl</span></div>
                            <div class="area_5_item shadow"><span>Photoshop</span></div>
                            <div class="area_5_item shadow"><span>PHP</span></div>
                            <div class="area_5_item shadow"><span>PostgreSQL</span></div>
                            <div class="area_5_item shadow"><span>Python</span></div>

                            <div class="area_5_item shadow"><span>Ruby</span></div>
                            <div class="area_5_item shadow"><span>Ruby on Rails</span></div>
                            <div class="area_5_item shadow"><span>SQL</span></div>
                            <div class="area_5_item shadow"><span>Unity</span></div>
                            <div class="area_5_item shadow"><span>WordPress</span></div>
                        </div>
                    </div>
                </div>
            </div>

            <div id="area_6_container">
                <div class="container" id="area_6">
                    <div class="row">
                        <h2 class="no_margin col-lg-12" id="area_6_title">Actual case example</h2>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <div id="area_6_detail">
                                <div class="area_6_detail_col1">
                                    <div class="area_6_item area_6_item_header">Application</div>
                                    <a class="area_6_item shadow" id="area_6_item_1" href="http://www.tadacopy.com/app/" title="" target="_blank">
                                        <div class="area_6_item_inner">
                                            <div class="area_6_item_icon crisp"></div>
                                            <div class="area_6_item_more">
                                                <div class="display_table">
                                                    <div class="display_row">
                                                        <div class="display_cell"><div></div></div>
                                                        <div class="display_cell"><span></span></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                    <a class="area_6_item shadow" id="area_6_item_4" href="http://teppan.email/lp" title="" target="_blank">
                                        <div class="area_6_item_inner">
                                            <div class="area_6_item_icon crisp"></div>
                                            <div class="area_6_item_more">
                                                <div class="display_table">
                                                    <div class="display_row">
                                                        <div class="display_cell"><div></div></div>
                                                        <div class="display_cell"><span></span></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                                <div class="area_6_detail_col2">
                                    <div class="area_6_item area_6_item_header">WEB</div>
                                    <a class="area_6_item shadow" id="area_6_item_2" href="http://lab.oceanize.co.jp/" title="" target="_blank">
                                        <div class="area_6_item_inner">
                                            <div class="area_6_item_icon crisp"></div>
                                            <div class="area_6_item_more">
                                                <div class="display_table">
                                                    <div class="display_row">
                                                        <div class="display_cell"><div></div></div>
                                                        <div class="display_cell"><span></span></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                    <a class="area_6_item shadow" id="area_6_item_5" href="http://smart-campus.jp/" title="" target="_blank">
                                        <div class="area_6_item_inner">
                                            <div class="area_6_item_icon crisp"></div>
                                            <div class="area_6_item_more">
                                                <div class="display_table">
                                                    <div class="display_row">
                                                        <div class="display_cell"><div></div></div>
                                                        <div class="display_cell"><span></span></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                    <a class="area_6_item shadow" id="area_6_item_7" href="http://cp.smart-campus.jp/" title="" target="_blank">
                                        <div class="area_6_item_inner">
                                            <div class="area_6_item_icon crisp"></div>
                                            <div class="area_6_item_more">
                                                <div class="display_table">
                                                    <div class="display_row">
                                                        <div class="display_cell"><div></div></div>
                                                        <div class="display_cell"><span></span></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                                <div class="area_6_detail_col3">
                                    <div class="area_6_item area_6_item_header">System development</div>
                                    <div class="area_6_item shadow" id="area_6_item_3"><div class="area_6_item_inner"><div class="area_6_item_icon crisp"></div></div></div>
                                    <div class="area_6_item shadow" id="area_6_item_6"><div class="area_6_item_inner"><div class="area_6_item_icon crisp"></div></div></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="container" id="area_7">
                <div class="row">
                    <h2 class="no_margin col-lg-12" id="area_7_title">
                      Why Bangladesh?
                        <span>
                            Our Team Sourcing (laboratory development) is specialized in Bangladesh engineers.
                        </span>
                    </h2>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div id="area_7_detail">
                            <div class="area_7_item shadow wow fadeInUp" id="area_7_item_1" data-wow-delay="0s">
                                <div class="area_7_item_number">1</div>
                                <div class="area_7_item_icon"></div>
                                <div class="area_7_item_text">
                                Because it was a British colony
                                The average score of English TOEIC is
                                About 900 points and the world top class
                                </div>
                            </div>
                            <div class="area_7_item shadow wow fadeInUp" id="area_7_item_2" data-wow-delay="0.3s">
                                <div class="area_7_item_number">2</div>
                                <div class="area_7_item_icon"></div>
                                <div class="area_7_item_text">
                                Medium- to long-term with 50% under 20
                                An increase in labor force is expected
                                </div>
                            </div>
                            <div class="area_7_item shadow wow fadeInUp" id="area_7_item_3" data-wow-delay="0.6s">
                                <div class="area_7_item_number">3</div>
                                <div class="area_7_item_icon"></div>
                                <div class="area_7_item_text">
                                Close to Indian people
                                Many science-based brain talents
                                </div>
                            </div>
                            <div class="area_7_item shadow wow fadeInUp" id="area_7_item_4" data-wow-delay="0.9s">
                                <div class="area_7_item_number">4</div>
                                <div class="area_7_item_icon"></div>
                                <div class="area_7_item_text">
                                  With developed population exceeding Japan
                                  Abundant talent resources
                                </div>
                            </div>
                            <div class="area_7_item shadow wow fadeInUp" id="area_7_item_5" data-wow-delay="1.2s">
                                <div class="area_7_item_number">5</div>
                                <div class="area_7_item_icon"></div>
                                <div class="area_7_item_text">
                                GDP per person
                                Only $ 1,033
                                The pressure to raise labor costs is low
                                </div>
                            </div>
                            <div class="area_7_item shadow wow fadeInUp" id="area_7_item_6" data-wow-delay="1.5s">
                                <div class="area_7_item_number">6</div>
                                <div class="area_7_item_icon"></div>
                                <div class="area_7_item_text">
                                    World-leading country Japan
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="container" id="area_8">
                <div class="row">
                    <h2 class="no_margin col-lg-12" id="area_8_title">
                    Team Sourcing (Laboratory Development)
                    It is introduced to such a company
                    </h2>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div id="area_8_detail">
                            <div class="area_8_item shadow wow fadeInLeft">
                                <div class="area_8_item_head">
                                    <div class="display_table">
                                        <div class="display_row">
                                            <div class="display_cell">

                                              Make your own service
                                              We are developing,
                                              Always maintenance and operation work
                                              There are a lot
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="area_8_item_foot">
                                    <div class="area_8_item_icon">
                                        <img src="img/icon_supporter.png" alt="自社サービス運営会社（IT系）"/>
                                    </div>
                                    <div class="area_8_item_text">
                                      In-house service
                                      Operating company (IT)
                                    </div>
                                </div>
                            </div>
                            <div class="area_8_item shadow">
                                <div class="area_8_item_head">
                                    <div class="display_table">
                                        <div class="display_row">
                                            <div class="display_cell">
                                            The number of entrusted cases increased
                                            There are several development projects,
                                            On the other hand,
                                            I have not caught up
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="area_8_item_foot">
                                    <div class="area_8_item_icon">
                                        <img src="img/icon_supporter.png" alt="システム受託開発会社"/>
                                    </div>
                                    <div class="area_8_item_text">
                                      system
                                      Contract development company
                                    </div>
                                </div>
                            </div>
                            <div class="area_8_item shadow wow fadeInRight">
                                <div class="area_8_item_head">
                                    <div class="display_table">
                                        <div class="display_row">
                                            <div class="display_cell">

                                              Development of in-house system
                                              Continue to ask external development company
                                              Outsourcing costs continue to increase
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="area_8_item_foot">
                                    <div class="area_8_item_icon">
                                        <img src="img/icon_supporter.png" alt="メーカー・事業会社"/>
                                    </div>
                                    <div class="area_8_item_text">
                                      Manufacturer · Operating company
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div id="area_9_container">
                <div class="container" id="area_9">
                    <div class="row">
                        <h2 class="no_margin col-lg-12" id="area_9_title">
                          Consultation · Quotation request is
                          Please feel free to contact us at OceanHawks.
                        </h2>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <div id="area_9_detail">
                                <a href="contact.php" onclick="addGaEvent(GA_TYPE.OPEN_INQUIRY_FORM)">
                                    <div class="display_table">
                                        <div class="display_row">
                                            <div class="display_cell"><div></div></div>
                                            <div class="display_cell"><span></span></div>
                                        </div>
                                    </div>
                                </a>
                                <div id="area_9_tel" class="wow bounce">
                                    <img src="img/tel.png" alt="オーシャンホークスへの問い合わせはこちらから"/>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="container" id="area_10">
                <div class="row">
                    <h2 class="no_margin col-lg-12" id="area_10_title">Company Profile</h2>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div id="area_10_detail">
                            <table>
                                <tr>
                                    <td class="area_10_col_1">【company name】</td>
                                    <td class="area_10_col_2">Ocean Hawks Co.Ltd.</td>
                                    <td class="area_10_col_3" rowspan="7" id="area_10_logo">
                                        <a href="">
                                            <img src="img/logo.png" alt="株式会社オーシャンホークス｜OceanHawks,inc."/>
                                        </a>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="area_10_col_1">【English notation】</td>
                                    <td>OceanHawks,inc.</td>
                                </tr>
                                <tr>
                                    <td class="area_10_col_1">【Establish】</td>
                                    <td>November 11, 2015</td>
                                </tr>
<!--                                <tr>
                                    <td class="area_10_col_1">【資本金】</td>
                                    <td>1億5,168万円(資本準備金含む)</td>
                                </tr>-->
                                <tr>
                                    <td class="area_10_col_1">【number of employees】</td>
                                    <td>65 employees (including Bangladesh employees)</td>
                                </tr>
                                <tr>
                                    <td class="area_10_col_1">【office】</td>
                                    <td>
                                      No. 9-1 Shimbashi 1 - chome, Minato - ku, Tokyo 105-0004
                                      Kitagawa Building 5th Floor</td>
                                </tr>
                                <tr>
                                    <td class="area_10_col_1">【Officer】</td>
                                    <td>
                                        Representative Director and President Tsutomu Koryo<br/><br/>
                                        Director Ryoji Tabara<br/><br/>
                                        Director Aikumi Iinuma
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

            <div class="container" id="area_footer">
                <div class="row">
                    <div class="col-lg-12">copyright ©  2016 OceanHawks</div>
                </div>
            </div>
        </div>

        <script type="text/javascript" src="js/jquery-2.1.3.min.js"></script>
        <script type="text/javascript" src="js/jquery-migrate-1.4.1.min.js"></script>
        <script type="text/javascript" src="js/bootstrap.min.js"></script>
        <script type="text/javascript" src="js/jquery-ui.min.js"></script>
        <script type="text/javascript" src="js/modernizr-custom.js"></script>
        <script type="text/javascript" src="js/jquery.kerning.min.js"></script>
        <script type="text/javascript" src="js/wow.min.js"></script>
        <script type="text/javascript" src="js/jquery.appear.js"></script>
        <script type="text/javascript" src="js/functions.js?20160913"></script>
        <script type="text/javascript" src="js/common.js?20160913"></script>
        <script>
            (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
            })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

            ga('create', 'UA-81271356-1', 'auto');
            ga('send', 'pageview');
        </script>
        <!-- Chamo script begin -->
        <script>
            var _chaq = _chaq || [];
            _chaq['_accountID']=2521;
            (function(D,s){
                var ca = D.createElement(s)
                ,ss = D.getElementsByTagName(s)[0];
                ca.type = 'text/javascript';
                ca.async = !0;
                ca.setAttribute('charset','utf-8');
                var sr = 'https://v1.chamo-chat.com/chamovps.js';
                ca.src = sr + '?' + parseInt((new Date)/60000);
                ss.parentNode.insertBefore(ca, ss);
            })(document,'script');
        </script>
        <!-- Chamo script end -->
    </body>
</html>
